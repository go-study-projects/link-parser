package main

import (
	"fmt"
	"io"
	"log"
	"os"

	parser "gitlab.com/go-study-projects/link-parser"
)

func main() {
	if len(os.Args) == 1 {
		log.Fatal("\nSpecify the file to parse as the first argument\n")
	}

	filepath := os.Args[1]
	file, err := os.Open(filepath)
	if err != nil {
		fmt.Println("File not found.")
		return
	}
	defer file.Close()

	reader := io.Reader(file)
	links, err := parser.Parse(reader)
	if err != nil {
		log.Fatal(err)
	}

	for _, link := range links {
		fmt.Printf("href: %s | text: %s\n", link.Href, link.Text)
	}
}

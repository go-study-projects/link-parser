package link_parser

import (
	"errors"
	"io"
	"strings"

	"golang.org/x/net/html"
)

func Parse(r io.Reader) ([]Link, error) {
	doc, err := html.Parse(r)
	if err != nil {
		return nil, errors.New("Parsing failed. Rekt.")
	}

	var links []Link
	getLinks(doc, &links)

	return links, nil
}

func getLinks(node *html.Node, links *[]Link) {
	if node.Type == html.ElementNode && node.Data == "a" {
		for _, attr := range node.Attr {
			if attr.Key == "href" {
				*links = append(*links, Link{
					Href: attr.Val,
					Text: getNodeText(node),
				})
			}
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		getLinks(c, links)
	}
}

func getNodeText(node *html.Node) string {
	var text string
	if node.Type == html.TextNode {
		return node.Data
	}

	for c := node.FirstChild; c != nil; c = c.NextSibling {
		text += getNodeText(c)
	}

	return strings.Join(strings.Fields(text), " ")
}

type Link struct {
	Href string
	Text string
}
